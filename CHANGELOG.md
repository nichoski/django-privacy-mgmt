## 0.0.21

- added optional translable fields description, lifetime, cookie

## 0.0.20

- fixed French translation

## 0.0.19

- added French translation
- blocktrans tags are now trimmed for better po file readability

## 0.0.18

- added missing migration
- improved docs

## 0.0.17

- Improved documentation
- django 2 compatibility
- removed 'null has no effect on ManyToManyField' warning

## 0.0.16

- Improved documentations

## 0.0.15

- German translations for most of the strings
