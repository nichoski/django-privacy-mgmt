��          �      L      �     �     �     �     �  '   �  
     m   %     �     �     �     �     �  
   �  "   �  d   �  �   ^  �   �     �  B  �          %     B     O  %   V     |  o   �  
   �                    %  	   A  (   K  �   t  �   �    �     �	                                             	                            
                           Accept Currently none in use. Description Domain Domain of the cookie (e.g. .domain.com) Essentials In order to improve your experience of our services, we will customise your experience based upon your usage. Lifetime Name Ordering Personalisation Privacy Settings Statistics The cookie's liftime (e.g. 1 Year) These cookies and scripts cannot be deactivated as they are needed to correctly render this website. These tools are used to collect statistics about user behaviour that helps us to improve our website and services. No personal data are collected. Websites do not have full control over cookies that may set by various third-party scripts. To see detailed information about and to manage cookies in your browser, please check its privacy settings. What's the cookie's purpose? Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Akzeptieren Derzeit nicht in Verwendung. Beschreibung Domain Domain des Cookies (z.B. .domain.com) Wesentliche Um Ihre Erfahrung mit unseren Services zu verbessern, passen wir Ihre Erfahrung basierend auf Ihrer Nutzung an. Lebenszeit Name Reihenfolge Personalisierung Privatsphäre-Einstellungen Statistik Die Lebenszeit des Cookies (z.B. 1 Jahr) Diese Cookies und Skripte können nicht deaktiviert werden, da sie für die korrekte Darstellung dieser Website benötigt werden. Diese Tools werden verwendet, um Statistiken über das Nutzerverhalten zu sammeln, die uns helfen, unsere Website und unsere Dienstleistungen zu verbessern. Es werden keine personenbezogenen Daten erhoben. Websites haben nicht die volle Kontrolle über Cookies, die von verschiedenen Skripten Dritter gesetzt werden können. Um detaillierte Informationen über die Verwaltung von Cookies in Ihrem Browser zu erhalten, überprüfen Sie bitte die Datenschutzeinstellungen. Wozu dient der Cookie? 