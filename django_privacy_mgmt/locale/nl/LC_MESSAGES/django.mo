��          �      �       H     I  	   P     Z  
   q  �   |  m        t     �  
   �  d   �  �     L   �  �   �  B  �  
   �     �     
  
   $  �   /  h   �     %     4     I  p   V  �   �  v   z  �   �               
                       	                           Accept Configure Currently none in use. Essentials If you do not consent to our use of cookies, please adjust your <a href="#" class="js-cookie-settings">privacy settings</a> accordingly.  In order to improve your experience of our services, we will customise your experience based upon your usage. Personalisation Privacy Settings Statistics These cookies and scripts cannot be deactivated as they are needed to correctly render this website. These tools are used to collect statistics about user behaviour that helps us to improve our website and services. No personal data are collected. This website uses cookies to help ensure that you enjoy the best experience. Websites do not have full control over cookies that may set by various third-party scripts. To see detailed information about and to manage cookies in your browser, please check its privacy settings. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Aanvaarden Zelf instellen Momenteel niet in gebruik Essentieel Begrijp je waar cookies voor dienen klik dan op “Aanvaarden”, of pas je voorkeuren voor cookies aan door “Zelf instellen” te kiezen. Op basis van je interacties met de website kunnen we je gepersonaliseerde inhoud en advertenties tonen.  Personalisatie Privacy-instellingen Statistieken Deze cookies en scripts kunnen niet worden uitgeschakeld omdat ze nodig zijn om de website goed te laten werken. Deze worden gebruikt om statistieken te verzamelen over gebruikersgedrag die ons helpen om onze website en diensten te verbeteren. Er worden geen persoonlijke gegevens verzameld. Deze website maakt gebruik van cookies om je surfervaring te verbeteren en je relevantere informatie te kunnen bieden. Websites hebben geen volledige controle over cookies die door verschillende scripts van derden kunnen worden geplaatst. Om gedetailleerde informatie te zien over cookies en deze te beheren in je browser, gelieve je privacy-instellingen te raadplegen. 