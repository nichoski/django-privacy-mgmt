# Generated by Django 2.2.14 on 2020-07-29 03:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('django_privacy_mgmt', '0002_auto_20190902_1118'),
    ]

    operations = [
        migrations.AddField(
            model_name='trackingitemtranslation',
            name='description',
            field=models.TextField(blank=True, help_text="What's the cookie's purpose?", null=True, verbose_name='Description'),
        ),
        migrations.AddField(
            model_name='trackingitemtranslation',
            name='domain',
            field=models.CharField(blank=True, help_text='Domain of the cookie (e.g. .domain.com)', max_length=1024, null=True, verbose_name='Domain'),
        ),
        migrations.AddField(
            model_name='trackingitemtranslation',
            name='lifetime',
            field=models.CharField(blank=True, help_text="The cookie's liftime (e.g. 1 Year)", max_length=255, null=True, verbose_name='Lifetime'),
        ),
    ]
